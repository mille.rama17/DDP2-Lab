package customer;
import movie.Movie;
import ticket.Ticket;
import theater.Theater;

public class Customer {
	private String name;
	private String gender;
	private int age;

	public Customer(String name, String gender, int age) {
		this.name = name;
		this.age = age;
	}

	public Ticket orderTicket(Theater theater, String title, String day, String threedi) {
		Movie film = checkTitle(theater, title);
		Ticket tiket = null;
		if (film != null) {
			tiket = checkTicket(theater, title, day, threedi);
			if (tiket != null) {
				if (checkRating(film, title)) {
					long price = checkPrice(day, threedi);
					System.out.println(this.name + " telah membeli tiket " + title + " jenis " + threedi + " di " + theater.getTheaterName() + " pada hari " + day + " seharga Rp. " + price);
					theater.setSaldo(theater.getSaldo() + price);
				} else {
					System.out.println(this.name + " masih belum cukup umur untuk menonton " + title + " dengan rating " + film.getRating());
				}
			} else {
				System.out.println("Tiket untuk film " + title + " jenis " + threedi + " dengan jadwal " + day + " tidak tersedia di " + theater.getTheaterName());
			}
		} else {
			System.out.println("Tiket untuk film " + title + " jenis " + threedi + " dengan jadwal " + day + " tidak tersedia di " + theater.getTheaterName());
		}	
		return tiket;
	}

	public void findMovie(Theater theater, String title) {
		Movie film = checkTitle(theater, title);
		if (film != null) {
			System.out.println("------------------------------------------------------------------\n" +
							   "Judul   : " + title + "\n" +
							   "Genre   : " + film.getGenre() + "\n" +
							   "Durasi  : " + film.getDuration() + " menit\n" +
							   "Rating  : " + film.getRating() + "\n" +
							   "Jenis   : Film " + film.getStatus() + "\n" +
							   "------------------------------------------------------------------");
		} else {
			System.out.println("Film " + title + " yang dicari " + this.name + " tidak ada di bioskop " + theater.getTheaterName());
		}
	}

	public Movie checkTitle(Theater theater, String title) {
		for (Movie i : theater.getMovie()) {
			if (i.getTitle().equals(title)) {
				return i;
			}
		}
		return null;
	}

	public Ticket checkTicket(Theater theater, String title, String day, String threedi) {
		boolean threeD;
		if (threedi.equals("3 Dimensi")) {
			threeD = true;
		} else {
			threeD = false;
		}
		for (Ticket i : theater.getTicket()) {
			if (i.getMovie().getTitle().equals(title) && i.getDay().equals(day) && i.getType() == threeD) {
				return i;
			}
		}
		return null;
	}

	public boolean checkRating(Movie movie, String title) {
		if (movie.getRating().equals("Umum")) {
			return true;
		} else if (movie.getRating().equals("Remaja")) {
			return this.age > 13;
		} else {
			return this.age > 17;
		}
	}

	public long checkPrice(String day, String threedi) {
		long price = 60000;
		if (day.equals("Sabtu") || day.equals("Minggu")) {
			price += 40000;
		}
		if (threedi.equals("3 Dimensi")) {
			return price * 6 / 5;
		}
		return price;
	}



}