package theater;
import java.util.ArrayList;
import movie.Movie;
import ticket.Ticket;

public class Theater {
	private String theaterName;
	private ArrayList<Ticket> ticket;
	private Movie[] movie;
	private long saldo;

	public Theater(String theaterName, int saldo, ArrayList<Ticket> ticket, Movie[] movie) {
		this.theaterName = theaterName;
		this.saldo = saldo;
		this.ticket = ticket;
		this.movie = movie;
	}

	public void setSaldo(long saldo) {
		this.saldo = saldo;
	}

	public long getSaldo() {
		return this.saldo;
	}

	public String getTheaterName() {
		return this.theaterName;
	}

	public Movie[] getMovie() {
		return movie;
	}

	public ArrayList<Ticket> getTicket() {
		return ticket;
	}

	public void printInfo() {
		System.out.print("------------------------------------------------------------------\n" +
			"Bioskop                 : " + this.theaterName + "\n" +
			"Saldo Kas               : " + this.saldo + "\n" +
			"Jumlah tiket tersedia   : " + this.ticket.size() + "\n" +
			"Daftar Film tersedia    : ");
		for (int i = 0; i < this.movie.length; i++) {
			if (i == this.movie.length - 1) {
				System.out.println(movie[i].getTitle());	
			} else {
				System.out.print(movie[i].getTitle() + ", ");
			}
		}
		System.out.println("------------------------------------------------------------------");
	}

	public static void printTotalRevenueEarned(Theater[] theaters) {
		int totalSaldo = 0;
		for (int i = 0; i < theaters.length; i++) {
			totalSaldo += theaters[i].getSaldo();
		}
		System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + totalSaldo + "\n" +
			"------------------------------------------------------------------");
			for (int i = 0; i < theaters.length; i++) {
				System.out.println("Bioskop     : " + theaters[i].getTheaterName() + "\n" +
								   "Saldo Kas   : Rp. " + theaters[i].getSaldo() + "\n");
			}
		System.out.println("------------------------------------------------------------------");		 
	}

	public void filterMovieByGenre (String genre) {
		String film = "";
		for (Movie i : this.getMovie()) {
			String input[] = i.getGenre().split("/");
			for (int x = 0; x < input.length; x++) {
				if (input[x].equals(genre)) {
					film += i.getTitle() + " ";
				}
			}
		}
		System.out.println(film);
	}
}