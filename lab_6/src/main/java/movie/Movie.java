package movie;

public class Movie {
	private String title;
	private String rating;
	private int duration;
	private  String genre;
	private  String status;

	public Movie(String title, String rating, int duration, String genre, String status) {
		this.title = title;
		this.rating = rating;
		this.duration = duration;
		this.genre = genre;
		this.status = status;
	} 		

	public String getTitle() {
		return title;
	}

	public String getRating() {
		return rating;
	}

	public int getDuration() {
		return duration;
	}

	public String getGenre() {
		return genre;
	}

	public String getStatus() {
		return status;
	}

	public String printMovie() {
		return "------------------------------------------------------------------\n" +
				"Judul   : " + this.title + "\n" +
				"Genre   : " + this.genre + "\n" +
				"Durasi  : " + this.duration + " menit\n" +
				"Rating  : " + this.rating + "\n" +
				"Jenis   : " + this.status + "\n" +
				"------------------------------------------------------------------";
	}
}