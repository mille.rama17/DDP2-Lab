package ticket;
import movie.Movie;

public class Ticket {
	public static int price = 60000;
	private Movie cinema;
	private String day;
	private boolean type;
	private String tridi;

	public Ticket(Movie cinema, String day, boolean type){
		this.cinema = cinema;
		this.type = type;
		this.day = day;
	}

	public int getPrice() {
		return this.price;
	}

	public boolean getType() {
		return this.type;
	}

	public String getDay() {
		return this.day;
	}

	public Movie getMovie() {
		return cinema;
	}

	public String printTicket() {
		return "------------------------------------------------------------------\n" +
				"Film            : " + cinema.getTitle() + "\n" +
				"Jadwal Tayang   : " + this.day + "\n" +
				"Jenis           : " + this.tridi + "\n" +
				"------------------------------------------------------------------";
	}
}