import java.util.Scanner;
/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 * @author Millenio Ramadizsa, NPM 1706040063, Kelas DDP2-D, GitLab Account: @mille.rama17
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Number[][] numbers = new Number[5][5];
		Number[] numberStates = new Number[100];
		BingoCard player = new BingoCard(numbers, numberStates);

		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				int numInput = input.nextInt();
				player.numbers[i][j] = new Number(numInput, i, j);
				player.numberStates[numInput] = player.numbers[i][j];
			}
			input.nextLine();
		}

		while (true) {
			String[] strInput = input.nextLine().split(" ");
			switch (strInput[0]) {
				case "MARK":
					System.out.println(player.markNum(Integer.parseInt(strInput[1])));
					break;
				case "INFO":
					System.out.println(player.info());
					break;
				case "RESTART":
					player.restart();
					break;
				default:
					System.out.println("Input anda salah!");
			}
			if (player.isBingo()) {
				System.out.println("BINGO!");
				System.out.println(player.info());
				break;
			}
		}
	}

	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		try {
			if (getNumberStates()[num].isChecked()) {
				return num + " sebelumnya sudah tersilang";
			} else {
				getNumberStates()[num].setChecked(true);
				for (int i = 0; i < 5; i++) {
					if (this.numbers[i][0].isChecked() && this.numbers[i][1].isChecked() &&
						this.numbers[i][2].isChecked() && this.numbers[i][3].isChecked() &&
						this.numbers[i][4].isChecked()) {
						this.isBingo = true;
						break;
					}
					if (this.numbers[0][i].isChecked() && this.numbers[1][i].isChecked() &&
						this.numbers[2][i].isChecked() && this.numbers[3][i].isChecked() &&
						this.numbers[4][i].isChecked()) {
					 	this.isBingo = true;
						break;
					}
					if (this.numbers[0][0].isChecked() && this.numbers[1][1].isChecked() &&
						this.numbers[2][2].isChecked() && this.numbers[3][3].isChecked() &&
						this.numbers[4][4].isChecked()) {
						this.isBingo = true;
						break;
					}
					if (this.numbers[4][0].isChecked() && this.numbers[3][1].isChecked() &&
						this.numbers[2][2].isChecked() && this.numbers[1][3].isChecked() &&
						this.numbers[0][4].isChecked()) {
						this.isBingo = true;
						break;
					}
				}
				return num + " tersilang";
			}
		} catch (NullPointerException e) {
			return "Kartu tidak memiliki angka " + num;
		}
	}	
	
	public String info(){
		String output = "";
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				output += "| ";
				if (getNumbers()[i][j].isChecked() == true) {
					output += "X ";
				} else {
					output += getNumbers()[i][j].getValue();
				}
				output += " "; 
			}
			if (i == 4) {
				output += "|";
			} else {
				output += "|\n";
			}
		}
		return output;
	}
	
	public void restart(){
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				getNumbers()[i][j].setChecked(false);
			}
		}
		System.out.println("Mulligan!");
	}


}
