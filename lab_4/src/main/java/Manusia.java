import java.util.ArrayList;
/* @author Millenio Ramadizsa, NPM 1706040063, Kelas DDP2-D, GitLab Account: @mille.rama17 */

public class Manusia {
    private String nama;
    private int umur;
    private int uang = 50000;
    private float kebahagiaan = 50;
    private boolean status = true;
    private final static float MinKebahagiaan = 0;
    private final static float MaxKebahagiaan = 100;

    public static ArrayList<Manusia> history = new ArrayList<>();

    public Manusia(String nama) {
        this.nama = nama;
        history.add(0, this);
    }

    public Manusia(String nama, int umur) {
        this.nama = nama;
        this.umur = umur;
        history.add(0, this);
    }

    public Manusia(String nama, int umur, int uang) {
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        history.add(0, this);
    }

     public Manusia(String nama, int umur, int uang, float kebahagiaan) {
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        this.kebahagiaan = kebahagiaan;
        history.add(0, this);
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public void setUang(int uang) {
        this.uang = uang;
    }

    public void setKebahagiaan(float kebahagiaan) {
        if (kebahagiaan > MaxKebahagiaan) {
            this.kebahagiaan = MaxKebahagiaan;
        } else if (kebahagiaan < MinKebahagiaan) {
            this.kebahagiaan = MinKebahagiaan;
        } else {
            this.kebahagiaan = kebahagiaan;
        }
    }

    public String getNama() {
        return nama;
    }

    public int getUmur() {
        return umur;
    }

    public int getUang() {
        return uang;
    }

    public float getKebahagiaan() {
        return kebahagiaan;
    }

    public boolean getStatus() {
        return status;
    }

    public void beriUang(Manusia tujuan) {
        if (this.status == false) {
            System.out.println(this.nama + " telah tiada");
        } else if (tujuan.getStatus() == false) {
            System.out.println(tujuan.getNama() + " telah tiada");
        } else {
            int totalAscii = 0;
            for (int i = 0; i < tujuan.getNama().length(); i++) {
                totalAscii += (int) tujuan.getNama().charAt(i);
            }
            int jumlahUang = totalAscii * 100;
            if (this.uang >= jumlahUang) {
                tujuan.setUang(tujuan.getUang() + jumlahUang);
                this.uang -= jumlahUang;
                tujuan.setKebahagiaan(tujuan.getKebahagiaan() + (float) jumlahUang / 6000);
                setKebahagiaan(this.kebahagiaan + (float) jumlahUang / 6000);
                System.out.println(this.nama + " memberi uang sebanyak " + jumlahUang + " kepada " + tujuan.getNama() + ", mereka berdua senang :D");
            } else if (this.uang < jumlahUang) {
                System.out.println(this.nama + " ingin memberi uang kepada " + tujuan.getNama() + " namun tidak memiliki cukup uang :'(");
            }
        }
    }

    public void beriUang(Manusia tujuan, int jumlahUang) {
        if (this.status == false) {
            System.out.println(this.nama + " telah tiada");
        } else if (tujuan.getStatus() == false) {
            System.out.println(tujuan.getNama() + " telah tiada");
        } else {
            if (this.uang >= jumlahUang) {
                tujuan.setUang(tujuan.getUang() + jumlahUang);
                this.uang -= jumlahUang;
                tujuan.setKebahagiaan(tujuan.getKebahagiaan() + (float) jumlahUang / 6000);
                setKebahagiaan(this.kebahagiaan + (float) jumlahUang / 6000);
                System.out.println(this.nama + " memberi uang sebanyak " + jumlahUang + " kepada " + tujuan.getNama() + ", mereka berdua senang :D");
            } else if (this.uang < jumlahUang) {
                System.out.println(this.nama + " ingin memberi uang kepada " + tujuan.getNama() + " namun tidak memiliki cukup uang :'(");
            }
        }
    }

    public void bekerja(int durasi, int bebanKerja) {
        if (this.status == false) {
            System.out.println(this.nama + " telah tiada");
        } else {
            if (this.umur < 18) {
                System.out.println(this.nama + " belum boleh bekerja karena masih dibawah umur D:");
            } else {
                int BebanKerjaTotal = durasi * bebanKerja;
                if (BebanKerjaTotal <= this.kebahagiaan) {
                    setKebahagiaan(this.kebahagiaan - BebanKerjaTotal);
                    int pendapatan = BebanKerjaTotal * 10000;
                    this.uang += pendapatan;
                    System.out.println(this.nama + " bekerja full time, total pendapatan : " + pendapatan);
                } else if (BebanKerjaTotal > this.kebahagiaan) {
                    int durasiBaru = (int) this.kebahagiaan / bebanKerja;
                    BebanKerjaTotal = durasiBaru * bebanKerja;
                    int pendapatan = BebanKerjaTotal * 10000;
                    setKebahagiaan(this.kebahagiaan - BebanKerjaTotal);
                    this.uang += pendapatan;
                    System.out.println(this.nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);
                }
            }
        }
    }

    public void rekreasi(String namaTempat) {
        if (this.status == false) {
            System.out.println(this.nama + " telah tiada");
        } else {
            int Biaya = namaTempat.length() * 10000;
            if (Biaya > this.uang) {
                System.out.println(this.nama + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat + " :(");
            } else {
                this.uang -= Biaya;
                setKebahagiaan(this.kebahagiaan + namaTempat.length());
                System.out.println(this.nama + " berekreasi di " + namaTempat + ", " + this.nama + " senang :)");
            }
        }
    }

    public void sakit(String namaPenyakit) {
        if (this.status == false) {
            System.out.println(this.nama + " telah tiada");
        } else {
            setKebahagiaan(this.kebahagiaan - namaPenyakit.length());
            System.out.println(this.nama + " terkena penyakit " + namaPenyakit + " :O");
        }
    }

    public String toString() {
        if (this.status == true) {
            return "Nama        : " + this.nama + "\nUmur        : " + this.umur + "\nUang        : " + this.uang + "\nKebahagiaan : " + this.kebahagiaan;
        } else {
            return "Nama        : Almarhum " + this.nama + "\nUmur        : " + this.umur + "\nUang        : " + this.uang + "\nKebahagiaan : " + this.kebahagiaan;
        }
        
    }

    public void meninggal() {
        if (this.status == false) {
            System.out.println(this.nama + " telah tiada");
        } else {
            this.status = false;
            if (history.size() == 1) {
                System.out.println("Semua harta " + history.get(0).getNama() + " hangus");
            } else {
                System.out.println(this.nama + " meninggal dengan tenang, kebahagiaan : " + this.kebahagiaan);
                history.get(0).setUang(history.get(0).getUang() + this.uang);
                this.uang = 0;
                System.out.println("Semua harta " + this.nama + " disumbangkan untuk " + history.get(0).getNama());
                for (int i = 0; i < history.size(); i++) {
                    if (history.get(i).equals(this)) {
                        history.remove(i);
                        break;
                    } else {
                        continue;
                    }
                }
            }
        }
    }
}