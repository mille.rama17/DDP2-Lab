import java.util.Scanner;
/* @author Millenio Ramadizsa, NPM 1706040063, Kelas DDP2-D, GitLab Account: @mille.rama17 */

public class RabbitHouse {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String rabbitInput = input.nextLine().trim();
        String namaRabbit = rabbitInput.split(" ")[1];
        String jenisInput = rabbitInput.split(" ")[0];
        int Rabbit = namaRabbit.length();
        if (jenisInput.equals("normal")) {
            if (Rabbit > 10) {
                System.out.println("Nama kelinci anda terlalu panjang!");
            } else {
                System.out.println(jumlahRabbit(Rabbit));
            }
        } else if (jenisInput.equals("palindrom")) {
            System.out.println(palindromRabbit(namaRabbit));
        } else {
            System.out.println("Perintah yang anda masukkan salah!");
        }
    }

    public static int jumlahRabbit(int Rabbit) {
        int RabbitOutput = 1;
        if (Rabbit == 1) {
            return 1;
        } else {
            RabbitOutput += Rabbit * jumlahRabbit(Rabbit - 1);
            return RabbitOutput;
        }
    }

    public static int palindromRabbit(String namaRabbit) {
        String reverseRabbit = "";
        int jumlahAnak = 0;
        for (int i = namaRabbit.length() - 1; i >= 0; i--) {
            reverseRabbit += namaRabbit.charAt(i);
        }
        if (namaRabbit.equals(reverseRabbit)) {
            return 0;
        } else {
            jumlahAnak += 1;
            for (int i = 0; i < namaRabbit.length(); i++) {
                String namaAnak = namaRabbit.substring(0, i) + namaRabbit.substring(i + 1);
                jumlahAnak += palindromRabbit(namaAnak);
            }
        }
        return jumlahAnak;
    }
}

