package character;
import character.Player;

public class Monster extends Player {
	private String roar = "AAAAAAaaaAAAAAaaaAAAAAA";

	public Monster(String name, int hp) {
		super(name, hp * 2);
		this.roar = roar;
		this.type = "Monster";
	}

	public Monster(String name, int hp, String roar) {
		super(name, hp * 2);
		this.roar = roar;
		this.type = "Monster";
	}

	public String roar() {
		return this.roar;
	}

}

//  write Monster Class here