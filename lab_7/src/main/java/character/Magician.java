package character;
import character.Player;

public class Magician extends Player {

	public Magician (String name, int hp) {
		super(name, hp);
		this.type = "Magician";
	}

	public String burn (Player victim) {
		if (victim.getIsDead() == true) {
                victim.setBurned();
                return "Nyawa " + victim.getName() + " " + victim.getHp() +
                "\n dan matang";
        } else {
            if (victim.getType().equals("Magician")) {
                victim.setHP(-20);
            } else {
                victim.setHP(-10);
            }
            if (victim.getIsDead() == true) {
                victim.setBurned();
                return "Nyawa " + victim.getName() + " " + victim.getHp() +
                "\n dan matang";
            } else {
                return "Nyawa " + victim.getName() + " " + victim.getHp();
            }
            
        }
	}
}

//  write Magician Class here