package character;
import java.util.ArrayList;

public class Player {
	protected String name;
	protected String type;
	protected int hp;
	protected boolean isDead;
	protected boolean isBurned;
	protected ArrayList<Player> eatenList = new ArrayList<Player>();

	public Player(String name, int hp) {
		this.name = name;
		this.type = type;
		this.isDead = false;
		if (hp > 0) {
			this.hp = hp;
		} else {
			this.hp = 0;
			this.setDead();
		}
		this.isBurned = false;
		this.eatenList = new ArrayList<>();
	}

	public String getName() {
		return this.name;
	}

	public String getType() {
		return this.type;
	}

	public int getHp() {
		return this.hp;
	}

	public void setHP(int hp) {
		if (this.hp + hp <= 0) {
			this.hp = 0;
			this.setDead();
		} else {
			this.hp = this.hp + hp;
		}
	}

	public void setBurned() {
		this.isBurned = true;
	}

	public boolean getIsBurned() {
		return this.isBurned;
	}

	public void setDead() {
		this.isDead = true;
	}

	public boolean getIsDead() {
		return this.isDead;
	}

	public void addEatenList(Player name) {
		eatenList.add(name);
	}

	public ArrayList<Player> getEatenList() {
		return this.eatenList;
	}

	public String attack(Player victim) {
		if (victim.getType().equals("Magician")) {
            victim.setHP(-20);
        } else {
            victim.setHP(-10);
        }
        return "Nyawa " + victim.getName() + " " + victim.getHp();
	}

	public boolean canEat(Player eaten) {
		if (this.type.equals("Human")) {
			if (eaten.getType().equals("Human")) {
				return false;
			} else if (eaten.getType().equals("Magician")) {
				return false;
			} else if (eaten.getType().equals("Monster")) {
				if (eaten.getIsDead() == false) {
					return false;
				} else if (eaten.getIsBurned() == false) {
					return false;
				} else {
					return true;
				}
			}
		} else if (this.type.equals("Magician")) {
			if (eaten.getType().equals("Human")) {
				return false;
			} else if (eaten.getType().equals("Magician")) {
				return false;
			} else if (eaten.getType().equals("Monster")) {
				if (eaten.getIsDead() == false) {
					return false;
				} else if (eaten.getIsBurned() == false) {
					return false;
				} else {
					return true;
				}
			}
		} else if (this.type.equals("Monster")) {
			if (eaten.getIsDead() == true) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

}



//  write Player Class here
