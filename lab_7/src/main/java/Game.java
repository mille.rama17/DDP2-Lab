import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    ArrayList<Player> eatenGame = new ArrayList<Player>();
    
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (Player i : player) {
            if (i.getName().equals(name)) {
                return i;
            } 
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        Player temp;
        if (find(chara) == null) {
            if (tipe.equals("Human")) {
                temp = new Human(chara, hp);
                player.add(temp);
            } else if (tipe.equals("Magician")) {
                temp = new Magician(chara, hp);
                player.add(temp);
            } else if (tipe.equals("Monster")) {
                temp = new Monster(chara, hp);
                player.add(temp);
            }   
            return chara + " ditambah ke game";
        } else {
            return "Sudah ada karakter bernama " + chara;
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        Player temp;
        if (tipe.equals("Monster")) {
            if (find(chara) == null) {
                temp = new Monster(chara, hp, roar);
                return chara + " ditambah ke game";    
            } else {
                return "Sudah ada karakter bernama " + chara;       
            }
        } else {
            return "bukan monster";
        }
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        if (find(chara) != null) {
            Player i = find(chara);
            player.remove(i);
            return chara + " dihapus dari game";
        } else {
            return "Tidak ada " + chara;
        }
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        String kondisi = "";
        if (find(chara) == null) {
            return "Tidak ada " + chara;
        } else {
            Player i = find(chara);
            if (i.getIsDead() == false) {
                kondisi = "Masih hidup";
            } else {
                kondisi = "Sudah meninggal dunia dengan damai";
            }
            return i.getType() + " " + i.getName() +
            "\nHP: " + i.getHp() +
            "\n" + kondisi +
            "\n" + ((i.getEatenList().size() == 0) ? "Belum memakan siapa siapa" : "Memakan " + diet(i.getName()));
        }        
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        if (player.size() == 0) {
            return "belom ada siapa siapa";
        } else {        
            String all = "";
            for (int i = 0; i < player.size(); i++) {
                all += status(player.get(i).getName()) + "\n";
            }
            return all;
        }
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        Player x;
        if (find(chara) == null) {
            return "Tidak ada " + chara;
        } else {
            String eaten = "";
            x = find(chara);
            for (int i = 0; i < x.getEatenList().size(); i++) {
                if (i == x.getEatenList().size() - 1) {
                    eaten += x.getEatenList().get(i).getType() + " " + x.getEatenList().get(i).getName();
                } else {
                    eaten += x.getEatenList().get(i).getType() + " " + x.getEatenList().get(i).getName() + ", ";
                }
            }
            return eaten;            
        }
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        if (eatenGame.size() == 0) {
            return "Belum memakan siapa siapa";
        } else {
            String eaten = "";
            for (int i = 0; i < eatenGame.size(); i++) {
                if (i == eatenGame.size() - 1) {
                    eaten += eatenGame.get(i).getType() + " " + eatenGame.get(i).getName();
                } else {
                    eaten += eatenGame.get(i).getType() + " " + eatenGame.get(i).getName() + ", ";
                }
            }
            return "Memakan" + eaten;
        }
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player attacker;
        Player victim;
        if (find(meName) != null) {
            attacker = find(meName);
            if (find(enemyName) != null) {
                victim = find(enemyName);
                if (attacker.getIsDead() == false) {                    
                    return attacker.attack(victim);
                } else {
                    return attacker.getName() + " tidak bisa menyerang " + victim.getName();
                }
            } else {
                return "Tidak ada " + enemyName;
            }
        } else {
            return "Tidak ada " + meName;
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        Player attacker;
        Player victim;
        if (find(meName) != null) {
            attacker = find(meName);
            if (find(enemyName) != null) {
                victim = find(enemyName);
                if (attacker.getIsDead() == false) {                    
                    if (attacker.getType().equals("Magician")) {
                        Magician x = (Magician)attacker;
                        return x.burn(victim);
                    } else {
                        return attacker.getName() + " tidak bisa membakar " + enemyName;
                    }
                } else {
                    return attacker.getName() + " tidak bisa membakar " + enemyName;
                }
            } else {
                return "Tidak ada " + enemyName;
            }
        } else {
            return "Tidak ada " + meName;
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player eater;
        Player eaten;
        if (find(meName) != null) {
            eater = find(meName);
            if (find(enemyName) != null) {
                eaten = find(enemyName);
                if (eater.getIsDead() ==  false) {                    
                    if (eater.canEat(eaten) == true) {
                        eater.setHP(15);
                        eater.addEatenList(eaten);
                        eatenGame.add(eaten);
                        player.remove(eaten);
                        return eater.getName() + " memakan " + enemyName +
                        "\nNyawa " + eater.getName() + " kini " + eater.getHp();
                    } else {
                        return eater.getName() + " tidak bisa memakan " + eaten.getName();
                    }
                } else {
                    return eater.getName() + " tidak bisa memakan " + eaten.getName(); 
                }
            } else {
                return "Tidak ada " + enemyName;
            }
        } else {
            return "Tidak ada " + meName;
        }
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        Player chara;
        if (find(meName) == null) {
            return "Tidak ada " + meName;
        } else {
            chara = find(meName);
            if (chara.getIsDead() == false) {                
                if (chara.getType().equals("Monster")) {
                    Monster x = (Monster)chara;
                    return x.roar();
                } else {
                    return meName + " tidak bisa berteriak";
                }
            } else {
                return meName + " tidak bisa berteriak";
            }
        }
    }

}