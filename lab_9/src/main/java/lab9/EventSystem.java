package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * method to find and access event
     * @param name of the event we want to find
     * @return event that has already been initialized, null if the event hasn't been initialized
     */
    public Event getEvent(String name) {
        for (Event i: events) {
            if (i.getName().equals(name)) return i;
        }
        return null;
    }

    /**
     * Method to add an event
     * @param name of the event
     * @param startTimeStr start time of the event in String
     * @param endTimeStr end time of the event in String
     * @param costPerHourStr of the event
     * @return String whether the event is successfully added or not
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
        if (getEvent(name) == null) {
            String[] arrayStart = startTimeStr.split("_");
            String[] arrayDate = arrayStart[0].split("-");
            String[] arrayTime = arrayStart[1].split(":");
            String[] arrayEnd = endTimeStr.split("_");
            String[] arrayEndDate = arrayEnd[0].split("-");
            String[] arrayEndTime = arrayEnd[1].split(":");
            GregorianCalendar beginning = new GregorianCalendar(Integer.parseInt(arrayDate[0]),
                    Integer.parseInt(arrayDate[1]) - 1, Integer.parseInt(arrayDate[2]), Integer.parseInt(arrayTime[0]),
                    Integer.parseInt(arrayTime[1]), Integer.parseInt(arrayTime[2]));
            GregorianCalendar ends = new GregorianCalendar(Integer.parseInt(arrayEndDate[0]),
                    Integer.parseInt(arrayEndDate[1]) - 1, Integer.parseInt(arrayEndDate[2]),
                    Integer.parseInt(arrayEndTime[0]), Integer.parseInt(arrayEndTime[1]),
                    Integer.parseInt(arrayEndTime[2]));
            BigInteger cost = new BigInteger(costPerHourStr);
            if (beginning.compareTo(ends) < 0) {
                Event x = new Event(name, beginning, ends, cost);
                events.add(x);
                return "Event " + name + " berhasil ditambahkan!";
            } else {
                return "Waktu yang diinputkan tidak valid!";
            }
        } else {
            return "Event " + name + " sudah ada!";
        }

    }

    /**
     * method to find and access user
     * @param name of the user we want to find
     * @return user that has already been initialized, null if user hasn't been initialized
     */
    public User getUser(String name) {
        for (User i: users) {
            if (i.getName().equals(name)) return i;
        }
        return null;
    }

    /**
     * method to add user
     * @param name of the user we want to add
     * @return String whether the user is successfully added or not
     */
    public String addUser(String name)
    {
        if (getUser(name) == null) {
            User x = new User(name);
            users.add(x);
            return "User " + name + " berhasil ditambahkan!";
        }
        return "User " + name + " sudah ada!";
    }

    /**
     * Method to register an event to user
     * @param userName is the name of the user
     * @param eventName is the name of the event that the user want to join
     * @return String whether the user is able to attend the event or not
     */
    public String registerToEvent(String userName, String eventName)
    {
        if (getUser(userName) != null || getEvent(eventName) != null) {
            if (getUser(userName) != null) {
                if (getEvent(eventName) != null) {
                    if (getUser(userName).addEvent(getEvent(eventName))) {
                        return userName + " berencana menghadiri " + eventName + "!";
                    } else {
                        return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
                    }
                } else {
                    return "Tidak ada acara dengan nama " + eventName + "!";
                }
            } else {
                return "Tidak ada pengguna dengan nama " + userName + "!";
            }
        } else {
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        }
    }
}