package lab9.event;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event>
{
    /** Name of event */
    private String name;
    /** Start time of the event */
    private GregorianCalendar beginning;
    /** End time of the event */
    private GregorianCalendar ends;
    /** Cost per hour time of the event */
    private BigInteger cost;

    /**
     * Consructor, initialize the start and finish of an event and cost per hour
     * @param name of the event
     * @param start of the event
     * @param end of the event
     * @param cost per hour of the event
     */
    public Event(String name, GregorianCalendar start, GregorianCalendar end, BigInteger cost) {
        this.name = name;
        this.cost = cost;
        this.beginning = start;
        this.ends = end;

    }
    
    // TODO: Make instance variables for representing beginning and end time of event
    
    // TODO: Make instance variable for cost per hour
    
    // TODO: Create constructor for Event class
    
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName()
    {
        return this.name;
    }

    /**
     * Accessor for cost
     * @return cost of this event instance
     */
    public BigInteger getCost() { return cost; }

    // TODO: Implement toString()

    /**
     * Method to print the event
     * @return Description of the event
     */
    @Override
    public String toString() {
        SimpleDateFormat result = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
        return this.name +
                "\nWaktu mulai: " + result.format(this.beginning.getTime()) +
                "\nWaktu selesai: " + result.format(this.ends.getTime()) +
                "\nBiaya kehadiran: " + this.cost;
    }

    /**
     * Method to check if the event overlaps with another event
     * @param party is another event which this is compared with
     * @return true if overlap, false if not overlap
     */
    public boolean overlap(Event party) {
        if (beginning.compareTo(party.beginning) < 0 && ends.compareTo(party.beginning) < 0) {
            return false;
        } else if (party.beginning.compareTo(beginning) < 0 && party.ends.compareTo(beginning) < 0) {
            return false;
        } else if (party.ends.compareTo(beginning) == 0 || ends.compareTo(party.beginning) == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Method to clone the object
     * @return the cloned object
     */
    public Event clonedEvent() {
        Event clone = new Event(this.name, this.beginning, this.ends, this.cost);
        return clone;
    }

    /**
     * Method overriding the compareTo method on the Comparable Interface
     * @param party is another event which this is compared to
     * @return < 0 when this is earlier than other, > 0 when other is earlier than this
     */
    @Override
    public int compareTo(Event party) {
        return beginning.compareTo(party.beginning);
    }


    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.
}
