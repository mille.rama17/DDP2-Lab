import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 * <p>
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 * <p>
 * Code Author (Mahasiswa):
 * @author Millenio Ramadizsa, NPM 1706040063, Kelas DDP2-D, GitLab Account: @mille.rama17
 */

public class SistemSensus {
    public static void main(String[] args) {
        // Buat input scanner baru
        Scanner input = new Scanner(System.in);

        String nama, alamat, tanggalLahir, catatanInput;
        short panjang, lebar, tinggi, jumlahCetakan;
        float berat;
        byte makanan;

        // TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
        // User Interface untuk meminta masukan
        try {
            System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
                    "--------------------\n" +
                    "Nama Kepala Keluarga   : ");
            nama = input.nextLine();

            System.out.print("Alamat Rumah           : ");
            alamat = input.nextLine();

            System.out.print("Panjang Tubuh (cm)     : ");
            panjang = Short.parseShort(input.nextLine());
            if (!(0 < panjang && panjang <= 250)) {
                System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
                return;
            }

            System.out.print("Lebar Tubuh (cm)       : ");
            lebar = Short.parseShort(input.nextLine());
            if (!(0 < lebar && lebar <= 250)) {
                System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
                return;
            }

            System.out.print("Tinggi Tubuh (cm)      : ");
            tinggi = Short.parseShort(input.nextLine());
            if (!(0 < tinggi && tinggi <= 250)) {
                System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
                return;
            }

            System.out.print("Berat Tubuh (kg)       : ");
            berat = Float.parseFloat(input.nextLine());
            if (!(0 < berat && berat <= 150)) {
                System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
                return;
            }

            System.out.print("Jumlah Anggota Keluarga: ");
            makanan = Byte.parseByte(input.nextLine());
            if (!(0 < makanan && makanan <= 20)) {
                System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
                return;
            }

            System.out.print("Tanggal Lahir          : ");
            tanggalLahir = input.nextLine();	

            System.out.print("Catatan Tambahan       : ");
            catatanInput = input.nextLine().trim();

            System.out.print("Jumlah Cetakan Data    : ");
            jumlahCetakan = Short.parseShort(input.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
            return;
        }

        // TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
        // TODO Hitung rasio berat per volume (rumus lihat soal)
        int rasio = (int) ((berat * 1000000) / (float) (panjang * lebar * tinggi));

        for (short i = 0; i < jumlahCetakan; i++) {
            // TODO Minta masukan terkait nama penerima hasil cetak data
            System.out.print("\nPencetakan " + (i + 1) + " dari " + jumlahCetakan + " untuk: ");
            String penerima = input.nextLine(); // Lakukan baca input lalu langsung jadikan uppercase
            String catatanOutput;
            // TODO Periksa ada catatan atau tidak
            if (!(catatanInput.length() == 0)) {
                catatanOutput = "catatan: " + catatanInput;
            } else {
                catatanOutput = "Tidak ada catatan tambahan";
            }

            // TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
            String hasil = nama + " - " + alamat + "\n" + "Lahir pada tanggal " + tanggalLahir + "\n" + "Rasio Berat Per Volume     = " + rasio + " kg/m^3" + "\n" + catatanOutput + "\n";
            System.out.print("\nDATA SIAP DICETAK UNTUK " + penerima.toUpperCase() + "\n--------------------\n" + hasil);
        }


        // TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
        // TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
        int totalAscii = 0;
        for (short i = 0; i < nama.length(); i++) {
            totalAscii += (int) nama.charAt(i);
        }
        int kalkukasi = (((panjang * tinggi * lebar) + totalAscii) % 10000);

        // TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
        String sKalkulasi = Integer.toString(kalkukasi);
        String nomorKeluarga = nama.charAt(0) + sKalkulasi;

        // TODO Hitung anggaran makanan per tahun (rumus lihat soal)
        int totalAnggaran = 50000 * 365 * (int) makanan;
        String anggaran = "Rp " + Integer.toString(totalAnggaran);

        // TODO Hitung umur dari tanggalLahir (rumus lihat soal)
        int tahunLahir = Integer.parseInt(tanggalLahir.split("-")[2]); // lihat hint jika bingung
        int umur = 2018 - tahunLahir;

        // TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
        String apartemen = "";
        String kabupaten = "";
        if (umur <= 18 && umur >= 0) {
            apartemen = "PPMT";
            kabupaten = "Rotunda";
        } else if (umur <= 1018 && umur >= 19) {
            if (totalAnggaran >= 0 && totalAnggaran <= 100000000) {
                apartemen = "Teksas";
                kabupaten = "Sastra";
            } else if (totalAnggaran >= 100000000) {
                apartemen = "Mares";
                kabupaten = "Margonda";
            }
        }

        // TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
        String rekomendasi = "\nREKOMENDASI APARTEMEN\n--------------------\nMENGETAHUI: Identitas Keluarga: " + nama + " - " + nomorKeluarga + "\n" + "MENIMBANG:  Anggaran makanan tahunan: "
                + anggaran + "\n" + "            Umur kepala keluarga: " + umur + " tahun\n" + "MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n" + apartemen + ", kabupaten " + kabupaten;
        System.out.print(rekomendasi);

        input.close();
    }
}