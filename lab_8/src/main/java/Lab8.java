import java.util.Scanner;
import java.util.ArrayList;
import korporasi.*;

class Lab8 {
	public static ArrayList<Karyawan> employee = new ArrayList<Karyawan>();

    public static void main(String[] args) {
    	Scanner input = new Scanner(System.in);
    	while (true) {		
	    	String strInput = input.nextLine();
	    	if (strInput.matches("[0-9]+")) {
	    		int limit = Integer.parseInt(strInput);
	    		Karyawan.setLimit(limit);
	    	} else {
	    		String arrayInput[] = strInput.split(" ");
	    		Karyawan temp;
	    		if (arrayInput[0].equals("TAMBAH_KARYAWAN")) {
	    			int gaji = Integer.parseInt(arrayInput[3]);
	    			if (find(arrayInput[1]) == null) {
			    		if (arrayInput[2].equals("INTERN")) {
			    			temp = new Intern(arrayInput[1], gaji);
			    			employee.add(temp);
			    		} else if (arrayInput[2].equals("STAFF")) {
			    			temp = new Staff(arrayInput[1], gaji);
			    			employee.add(temp);
			    		} else if (arrayInput[2].equals("MANAGER")) {
			    			temp = new Manager(arrayInput[1], gaji);
			    			employee.add(temp);
			    		}
			    		System.out.println(arrayInput[1] + " mulai bekerja sebagai " + arrayInput[2] + " di PT. TAMPAN");	
			    	} else {
			    		System.out.println("Karyawan dengan nama " + arrayInput[1] + " telah terdaftar");
			    	}
	    		} else if (arrayInput[0].equals("STATUS")) {
	    			System.out.println(status(arrayInput[1]));
	    		} else if (arrayInput[0].equals("TAMBAH_BAWAHAN")) {
	    			if (find(arrayInput[1]) != null) {
	    				Karyawan boss = find(arrayInput[1]);
	    				if (find(arrayInput[2]) != null) {
	    					Karyawan subordinate = find(arrayInput[2]);
	    					if (boss.getType().equals("Manager")) {
	    						Manager boss1 = (Manager)boss;
	    						if (boss1.getUnderling().size() == 10) {
	    							System.out.println("Anda sudah tidak layak memiliki lebih banyak bawahan");
	    						} else {
	    							System.out.println(boss1.addUnderling(subordinate));					
	    						}	
	    					} else if (boss.getType().equals("Staff")) {
	    						Staff boss2 = (Staff)boss;
	    						if (boss2.getUnderling().size() == 10) {
	    							System.out.println("Anda sudah tidak layak memiliki lebih banyak bawahan");
	    						} else {
	    							System.out.println(boss2.addUnderling(subordinate));
	    						}
	    					} else if (boss.getType().equals("Intern")) {
	    						System.out.println("Anda tidak layak memiliki bawahan");
	    					}
	    				} else {
	    					System.out.println("Nama tidak berhasil ditemukan");	
	    				}
	    			} else {
	    				System.out.println("Nama tidak berhasil ditemukan");
	    			}
	    		} else if (arrayInput[0].equals("GAJIAN")) {
	    			System.out.println(gajian());
	    			for (int i = 0; i < employee.size(); i++) {
	    				if (employee.get(i).getNumFee() % 6 == 0 && employee.get(i).getNumFee() != 0) {
	    					System.out.print(employee.get(i).getName() + " mengalami kenaikan gaji sebesar 10% dari " + employee.get(i).getFee());
	    					employee.get(i).setFee();
	    					System.out.println(" menjadi " + employee.get(i).getFee());
	    				}
	    				if (employee.get(i).getType().equals("Staff")) {
	    					if (employee.get(i).getFee() > Karyawan.getLimit()) {
	    						System.out.println("Selamat, " + employee.get(i).getName() + " telah dipromosikan menjadi MANAGER");
	    						Karyawan x = new Manager(employee.get(i).getName(), employee.get(i).getFee());
	    						Staff in = (Staff)employee.get(i);
	    						Manager y = (Manager)x;
	    						for (Karyawan j: in.getUnderling()) {
	    							y.addUnderling(j);
	    						}
	    						employee.set(i, x);
	    					}
	    				}
	    			}
	    		} else if (arrayInput[0].equals("SELESAI")) {
	    			break;
	    		}
	    	}
    	}
    }

    public static Karyawan find(String name) {
    	for (Karyawan i : employee) {
    		if (i.getName().equals(name)) {
    			return i;
    		}
    	}
    	return null;
    }

    public static String status(String name) {
    	if (find(name) != null) {
    		Karyawan i = find(name);
    		return name + " " + i.getFee();
    	} else {
    		return "Karyawan tidak ditemukan";
    	}
    }

    public static String gajian() {
    	for (Karyawan i : employee) {
    		i.setNumFee();	
    	}
    	return "Semua karyawan telah diberikan gaji";
    }

}