import korporasi.*;
import java.util.ArrayList;

public class Korporasi {
	public static ArrayList<Karyawan> employee = new ArrayList<Karyawan>();

	public Karyawan find(String name) {
    	for (Karyawan i : employee) {
    		if (i.getName().equals(name)) {
    			return i;
    		}
    	}
    	return null;
    }

    public String status(String name) {
    	if (find(name) != null) {
    		Karyawan i = find(name);
    		return name + " " + i.getFee();
    	} else {
    		return "Karyawan tidak ditemukan";
    	}
    }

    public String gajian() {
    	for (Karyawan i : employee) {
    		i.setNumFee();	
    	}
    	return "Semua karyawan telah diberikan gaji";
    }

    public String add(String name, int fee, String type) {
    	if (find(name) == null) {
			if (arrayInput[2].equals("INTERN")) {
			    temp = new Intern(arrayInput[1], gaji);
			    employee.add(temp);
			} else if (arrayInput[2].equals("STAFF")) {
			    temp = new Staff(arrayInput[1], gaji);
			    employee.add(temp);
			} else if (arrayInput[2].equals("MANAGER")) {
			    temp = new Manager(arrayInput[1], gaji);
			    employee.add(temp);
			}
			    System.out.println(arrayInput[1] + " mulai bekerja sebagai " + arrayInput[2] + " di PT. TAMPAN");	
			} else {
			    System.out.println("Karyawan dengan nama " + arrayInput[1] + " telah terdaftar");
			}
    }
}