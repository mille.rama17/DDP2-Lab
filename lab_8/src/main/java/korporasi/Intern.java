package korporasi;
import korporasi.Karyawan;

public class Intern extends Karyawan {
	public Intern(String name, int fee) {
		super(name);
		this.fee = fee;
		this.type = "Intern";
	}
}