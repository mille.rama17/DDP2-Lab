package korporasi;
import korporasi.Karyawan;
import java.util.ArrayList;

public class Manager extends Karyawan {
	protected ArrayList<Karyawan> underling = new ArrayList<Karyawan>();

	public Manager(String name, int fee) {
		super(name);
		this.fee = fee;
		this.type = "Manager";
	}

	public ArrayList<Karyawan> getUnderling() {
		return this.underling;
	}

	public Karyawan findUnderling(String name) {
		for (Karyawan i : underling) {
			if (i.getName().equals(name)) {
				return i;
			}
		}
		return null;
	}

	public String addUnderling(Karyawan subordinate) {
		if (subordinate.getType().equals("Manager")) {
			return "Anda tidak layak memiliki bawahan";
		} else {
			if (findUnderling(subordinate.getName()) == null) {
				underling.add(subordinate);
				return "Karyawan " + subordinate.getName() + " berhasil ditambahkan menjadi bawahan " + this.getName();
			} else {
				return "Karyawan " + subordinate.getName() + " telah menjadi bawahan " + this.getName();
			}
		}
	}

}