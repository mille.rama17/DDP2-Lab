package korporasi;
import korporasi.Karyawan;
import java.util.ArrayList;

public class Staff extends Karyawan {
	protected ArrayList<Karyawan> underling = new ArrayList<Karyawan>();

	public Staff(String name, int fee) {
		super(name);
		this.type = "Staff";
		this.fee = fee;
	}

	public Karyawan findUnderling(String name) {
		for (Karyawan i : underling) {
			if (i.getName().equals(name)) {
				return i;
			}
		}
		return null;
	}

	public ArrayList<Karyawan> getUnderling() {
		return this.underling;
	}
	
	public String addUnderling(Karyawan subordinate) {
		if (subordinate.getType().equals("Manager") && subordinate.getType().equals("Staff")) {
			return "Anda tidak layak memiliki bawahan";
		} else {
			if (findUnderling(subordinate.getName()) == null) {
				underling.add(subordinate);
				return "Karyawan " + subordinate.getName() + " berhasil ditambahkan menjadi bawahan " + this.getName();
			} else {
				return "Karyawan " + subordinate.getName() + " telah menjadi bawahan " + this.getName();
			}
		}
	}
}