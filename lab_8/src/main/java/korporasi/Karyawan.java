package korporasi;

public class Karyawan {
	protected String name;
	protected String type;
	protected int fee;
	protected int numFee = 0;
	protected static int limit = 18000;

	public Karyawan(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public int getFee() {
		return this.fee;
	}

	public void setFee() {
		this.fee = (this.fee * 110) / 100;
	}

	public String getType() {
		return this.type;
	}

	public void setNumFee() {
		this.numFee++;
	}

	public int getNumFee() {
		return this.numFee;
	}

	public static void setLimit(int newLimit){
		limit = newLimit;
	}

	public static int getLimit() {
		return limit;
	}

}