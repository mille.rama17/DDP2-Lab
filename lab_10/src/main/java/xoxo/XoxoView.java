package main.java.xoxo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Millenio Ramadizsa
 */
public class XoxoView {
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    /**
     * A field that for inputting the seed
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    /**
     * Frame for gui display
     */
    private JFrame frame = new JFrame("xoxo message decryptor and encryptor");

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        JPanel input = new JPanel(new GridLayout(3 , 1));
        JPanel message = new JPanel(new FlowLayout());
        messageField = new JTextField();
        messageField.setPreferredSize(new Dimension(200, 25));
        JLabel messageLabel = new JLabel("Message : ");
        message.add(messageLabel);
        message.add(messageField);
        JPanel key = new JPanel(new FlowLayout());
        keyField = new JTextField();
        keyField.setPreferredSize(new Dimension(200, 25));
        JLabel keyLabel = new JLabel("Key : ");
        key.add(keyLabel);
        key.add(keyField);
        JPanel seed = new JPanel(new FlowLayout());
        seedField = new JTextField();
        seedField.setPreferredSize(new Dimension(200,25));
        JLabel seedLabel = new JLabel("Seed : ");
        seed.add(seedLabel);
        seed.add(seedField);
        input.add(message);
        input.add(key);
        input.add(seed);
        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.setPreferredSize(new Dimension(200,200));
        encryptButton = new JButton("Encrypt");
        decryptButton = new JButton("Decrypt");
        encryptButton.setPreferredSize(new Dimension(100, 40));
        decryptButton.setPreferredSize(new Dimension(100, 40));
        buttonPanel.add(encryptButton, BorderLayout.LINE_START);
        buttonPanel.add(decryptButton, BorderLayout.LINE_END);
        logField = new JTextArea("Log \n");
        logField.setPreferredSize(new Dimension(300, 300));
        frame.add(input, BorderLayout.PAGE_START);
        frame.add(buttonPanel, BorderLayout.CENTER);
        frame.add(logField, BorderLayout.PAGE_END);
        frame.setVisible(true);
        frame.setSize(500, 500);
        frame.setResizable(false);
    }

    /**
     * method to open new JOptionPane to show that there is wrong input
     * @param message to show when there is wrong input
     */
    public void wrongInput(String message) {
        JOptionPane.showMessageDialog(frame, message);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets seed text from the seed field
     * @return seed input frome seedfield
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}