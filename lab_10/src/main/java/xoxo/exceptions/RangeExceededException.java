package main.java.xoxo.exceptions;

/**
 * <write the documentation>
 */
public class RangeExceededException extends RuntimeException {

    public RangeExceededException(String message) {
        super(message);
    }

}