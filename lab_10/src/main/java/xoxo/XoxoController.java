package main.java.xoxo;

import main.java.xoxo.crypto.XoxoDecryption;
import main.java.xoxo.crypto.XoxoEncryption;
import main.java.xoxo.exceptions.InvalidCharacterException;
import main.java.xoxo.exceptions.KeyTooLongException;
import main.java.xoxo.exceptions.RangeExceededException;
import main.java.xoxo.exceptions.SizeTooBigException;
import main.java.xoxo.util.XoxoMessage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static main.java.xoxo.key.HugKey.DEFAULT_SEED;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    private int fileEn = 1;
    private int fileDe = 1;

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setDecryptFunction(actions -> decryption());
        gui.setEncryptFunction(actions -> encryption());
    }

    /**
     * method to encrypt the input
     */
    public void encryption() {
        String message = gui.getMessageText();
        String kissKey = gui.getKeyText();
        String seed = gui.getSeedText();
        XoxoEncryption encryptMessage = null;
        try {
            encryptMessage = new XoxoEncryption(kissKey);
        } catch (KeyTooLongException e) {
            gui.wrongInput("Key length cannot exceed 28");
            return;
        } catch (InvalidCharacterException e) {
            gui.wrongInput("Key can only contain letter and '@'");
            return;
        }
        XoxoMessage enMessage = null;
        try {
            if (seed.equals("") || Integer.parseInt(seed) == 18) {
                enMessage = encryptMessage.encrypt(message);
                seed = "DEFAULT_SEED";
            } else {
                enMessage = encryptMessage.encrypt(message, Integer.parseInt(seed));
            }
        } catch (RangeExceededException e) {
            gui.wrongInput("seed input invalid");
            return;
        } catch (NumberFormatException e) {
            gui.wrongInput("seed must be a number");
            return;
        } catch (SizeTooBigException e) {
            gui.wrongInput("Size too big");
            return;
        }
        createEnFile(enMessage, seed);
    }

    /**
     * method to decrypt input
     */
    public void decryption() {
        String hugKey = gui.getKeyText();
        String message = gui.getMessageText();
        XoxoDecryption decryptMessage = new XoxoDecryption(hugKey);
        int seed;
        if (gui.getSeedText().equals("") || gui.getSeedText().equals("DEFAULT_SEED")) {
            seed = DEFAULT_SEED;
        } else {
            seed = Integer.parseInt(gui.getSeedText());
        }
        String deMessage = decryptMessage.decrypt(message, seed);
        createDeFile(deMessage);
    }

    /**
     * method to create encryption file
     * @param message that is going to be in the file
     * @param seed from input
     */
    public void createEnFile(XoxoMessage message, String seed) {
        File file = new File("D:\\Semester 2\\DDP 2\\DDP_Lab_1\\lab_10\\src\\main\\java\\encryptedFile\\"
                + fileEn + ".enc");
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(message.getEncryptedMessage());
            writer.flush();
            writer.close();
            gui.appendLog("Encrypted Text : " + message.getEncryptedMessage());
            gui.appendLog("Hug Key : " + message.getHugKey().getKeyString());
            gui.appendLog("Seed : " + seed);
            fileEn++;
        } catch (IOException e) {
            gui.appendLog("Encryption Failed");
        }
    }

    /**
     * method to create decryption file
     * @param message that is going to be in the file
     */
    public void createDeFile(String message) {
        File file = new File("D:\\Semester 2\\DDP 2\\DDP_Lab_1\\lab_10\\src\\main\\java\\decryptedFile\\"
                + fileDe + ".txt");
        try {
            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(message);
            writer.flush();
            writer.close();
            gui.appendLog("Decrypted Text : " + message);
            fileDe++;
        } catch (IOException e) {
            gui.appendLog("Decryption Failed");
        }
    }
}