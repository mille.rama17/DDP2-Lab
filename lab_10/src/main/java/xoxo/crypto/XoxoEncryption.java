package main.java.xoxo.crypto;

import main.java.xoxo.exceptions.KeyTooLongException;
import main.java.xoxo.exceptions.InvalidCharacterException;
import main.java.xoxo.exceptions.SizeTooBigException;
import main.java.xoxo.exceptions.RangeExceededException;
import main.java.xoxo.key.HugKey;
import main.java.xoxo.key.KissKey;
import main.java.xoxo.util.XoxoMessage;

import static main.java.xoxo.key.HugKey.MAX_RANGE;
import static main.java.xoxo.key.HugKey.MIN_RANGE;

/**
 * This class is used to create an encryption instance
 * that can be used to encrypt a plain text message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoEncryption {

    /**
     * A Kiss Key object that is required to encrypt the message.
     */
    private KissKey kissKey;
    private static int maxbyte = 1250;

    /**
     * Class constructor with the given Kiss Key
     * string to build the Kiss Key object.
     * 
     * @throws KeyTooLongException if the length of the
     *         kissKeyString exceeded 28 characters.
     */
    public XoxoEncryption(String kissKeyString) throws KeyTooLongException, InvalidCharacterException{
        this.kissKey = new KissKey(kissKeyString);
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     */
    public XoxoMessage encrypt(String message) {
        String encryptedMessage = this.encryptMessage(message); 
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey));
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @param seed A number to generate different Hug Key.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     * @throws RangeExceededException if seed is below 0 and above max_range
     */
    public XoxoMessage encrypt(String message, int seed) throws RangeExceededException {
        if (seed < MIN_RANGE || seed > MAX_RANGE) {
            throw new RangeExceededException("seed is not valid");
        }
        String encryptedMessage = this.encryptMessage(message); 
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey, seed));
    }

    /**
     * Runs the encryption algorithm to turn the message string
     * into an ecrypted message string.
     * 
     * @param message The message that wants to be encrypted.
     * @return The encrypted message string.
     * @throws SizeTooBigException if the size is more than 10 kbit
     * @throws InvalidCharacterException if kisskey contains other than letter and '@'
     */
    private String encryptMessage(String message) throws SizeTooBigException, InvalidCharacterException {
        if (message.getBytes().length > maxbyte) {
            throw new SizeTooBigException("message is bigger than 10kb");
        }
        final int length = message.length();
        String encryptedMessage = "";
        for (int i = 0; i < length; i++) {
            int m = message.charAt(i);
            int k = this.kissKey.keyAt(i) - 'a';
            int value = m ^ k;
            encryptedMessage += (char) value;
        }
        return encryptedMessage;
    }
}

